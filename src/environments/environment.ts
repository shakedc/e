// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyAGXAKliQlPv77GfZiWcntKkaFjqaWTEm4',
    authDomain: 'examc-93abc.firebaseapp.com',
    databaseURL: 'https://examc-93abc.firebaseio.com',
    projectId: 'examc-93abc',
    storageBucket: '',
    messagingSenderId: '137260300173',
    appId: '1:137260300173:web:433526eafc49cd3d496fc4'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
