import { Injectable } from '@angular/core';

import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  // addItem(name, write) {
  //   this.Auth.user.subscribe(value => {
  //     let ref = this.db.database.ref('/');
  //     ref.child('users').child(value.uid).child('items').push({'text': name, 'write': write, 'done': false});
  //   });
  // }
  // updateItem(key, name: string, write: string, done: boolean) {
  //   this.Auth.user.subscribe(value => {
  //     let ref = this.db.database.ref('/');
  //     this.db.list('/users/' + value.uid + '/items/').update(key, {'text': name, 'write': write, 'done': done});
  //   });
  // }

  constructor(private Auth: AngularFireAuth, private db: AngularFireDatabase ) { }
}
