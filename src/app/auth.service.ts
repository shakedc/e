import { Injectable } from '@angular/core';

import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<firebase.User>;

  constructor(private fireBaseAuth: AngularFireAuth, private db: AngularFireDatabase) {
    this.user = fireBaseAuth.authState;
   }

  logout() {
  return this.fireBaseAuth.auth.signOut();
  }
  login(email: string, password: string) {
  return this.fireBaseAuth.auth.signInWithEmailAndPassword(email, password);
  }


    insert(user, email: string, street: string, appartment: string, city: string) {
      let UID = user.uid;
      let REF = this.db.database.ref('/users/');
      REF.child(UID).push({'email': email, 'street': street, 'appartment': appartment, 'city': city});
    }

  signup(email: string, password: string) {
    return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email, password) ;
  }



  updateUser(user, name: string) {
        user.updateProfile({displayName: name, photoURL: ''});

    }
}
