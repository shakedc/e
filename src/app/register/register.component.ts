import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

street = '';
appartment = '';
city = '';

email = '';
password = '';
password2 = '';


show1 = false;
show2 = false;
show3 = false;
show4 = false;
show5 = false;

clicked1: false;
code = '';
msg = '';
refil = false;

  constructor(private auth: AuthService, private router: Router) { }
  signup() {
    if (this.city == '') {
      this.show5 = true;
    }
    else {
      this.show5 = false;
    }
    if (this.street == '') {
      this.show1 = true;
    }
    else {
      this.show1 = false;
    }
    if (this.appartment == '') {
      this.show2 = true;
    }
    else {
      this.show2 = false;
    }
    if(this.email == '') {
      this.show3 = true;
    }
    else{
      this.show3 = false;
    }
    if(this.password == '') {
      this.show4 = true;
    }
    else {
      this.show4 = false;
    }
    if (this.password2 == this.password) {
    this.refil = false;
    }
    else {
    this.refil = true;
    }

    if (this.show1 == false && this.show2 == false && this.show3 == false 
      && this.show4 == false && this.show5 == false) {
    this.auth.signup(this.email, this.password).then(value => {

      this.auth.updateUser(value.user, this.email);
      this.auth.insert(value.user, this.email, this.street, this.appartment, this.city)


    }).then(value => {
      this.router.navigate(['payments']); }).catch(err => {
      this.code = err.code;
      this.msg = err.message;

    });

  }}

  ngOnInit (){
    this.clicked1 = false;
  }
}



