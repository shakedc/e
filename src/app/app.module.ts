import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// components added by me
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutComponent } from './about/about.component';
import { MainComponent } from './main/main.component';
import { PnfoundComponent } from './pnfound/pnfound.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';

// router
import { Routes, RouterModule } from '@angular/router';

// material + forms
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ReactiveFormsModule} from '@angular/forms';
import { FormsModule} from '@angular/forms';

import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDividerModule} from '@angular/material/divider';

// imports for firebase
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { PaymentComponent } from './payment/payment.component';
import { PaymentsComponent } from './payments/payments.component';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    RegisterComponent,
    AboutComponent,
    MainComponent,
    PnfoundComponent,
    ItemsComponent,
    ItemComponent,
    PaymentComponent,
    PaymentsComponent
  ],
  imports: [
    BrowserModule,

    // routing
    RouterModule.forRoot([
          {path: '', component: MainComponent},
          {path: 'main', component: MainComponent},
          {path: 'login', component: LoginComponent},
          {path: 'register', component: RegisterComponent},
          {path: 'about', component: AboutComponent},
          {path: 'items', component: ItemsComponent},
          {path: 'payments', component: PaymentsComponent},
          {path: '**', component: PnfoundComponent}
        ]),

    // material + forms
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,

    MatButtonToggleModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatDividerModule,

    // firebase
    AngularFireModule.initializeApp(environment.firebaseConfig, 'e'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule, //  database features
    AngularFireStorageModule // for storage features


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
