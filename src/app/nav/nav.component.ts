import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router: Router, public auth: AuthService) { }

  ngOnInit() {
  }
  toM() {
    this.router.navigate(['/main']);
  }
  toL() {
    this.router.navigate(['/login']);
  }
  toR() {
    this.router.navigate(['/register']);
  }
  toA() {
    this.router.navigate(['/about']);
  }
  toLogout() {
    this.auth.logout()
    .then(value => {
      this.router.navigate(['/pnfound']);
    });
  }
  toMI() {
    this.router.navigate(['/items']);
  }
  toP() {
    this.router.navigate(['/payments']);
  }
}
