import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {

  constructor(private router: Router, public auth: AuthService, private db: AngularFireDatabase) { }


str: '';
num: '';
cit: '';



  pay() {
    this.router.navigate(['/items']);
  }

  ngOnInit() {

  }
}
